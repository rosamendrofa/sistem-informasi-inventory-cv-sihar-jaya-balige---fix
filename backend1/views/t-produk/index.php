<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\TProdukSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Stok';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tproduk-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>Laporan Stok</p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kode_produk',
            'id_kategori',
            'nama_produk',
            'status_produk',
            'harga_jual',
            //'harga_beli',
            //'jumlah_produk',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?= Html::a('Cetak', ['create'], ['class' => 'btn btn-success']) ?>

</div>
